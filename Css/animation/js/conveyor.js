function Conveyor(conveyorId, numDisplayItems, duration, delay){
	this.conveyorId = conveyorId;
	this.numDisplayItems = numDisplayItems;
	this.duration = duration;
	this.delay = delay;
	
	var items = [];
	var itemHeight;
	var itemWidth;
	
	// function
	this.getItems = function(){
		this.items = $("#" + this.conveyorId).children();
		var length = this.items.length;
		if(length == 0){
			return;
		}
		if(length < 3){
			while(this.items.length < 3){
				for(var i = 0; i < length; i++){
					this.items.push(this.items[i]);
				}
			}
		}
		if(this.numDisplayItems == undefined){
			this.numDisplayItems = this.items.length - 2;
		}else{
			if(isNaN(this.numDisplayItems)){
				this.numDisplayItems = this.items.length - 2;
			}else{
				if(this.numDisplayItems > this.items.length - 2){
					this.numDisplayItems = this.items.length - 2;
				}
			}
		}
		console.log(this.numDisplayItems);
	}
	this.setStyleItems = function(){
		if(this.items!= null){
			this.itemHeight = $(this.items[0]).height() ;
			this.itemWidth = $(this.items[0]).width() ;
			var style = "height : " + this.itemHeight + "px; width : " + this.itemWidth + 
			"px; top : " + (-this.itemHeight) + "px;" + "position: absolute;";
			$(this.items).each(function(){
				$(this).attr("style", style);
			})	
		}
	}
	
	this.setStyleConveyor = function(){
		$("#" + this.conveyorId).attr("style", "position: relative;" + 
			"height : " + this.numDisplayItems * this.itemHeight + "px; width:" + this.itemWidth + "px; overflow : hidden");
	}
	this.setKeyFrameItems = function(){
		var length = this.items.length;
		var percent = 100 / ( length);
		var height = this.itemHeight;
		var keyFrames = '\n<style type="text/css">\n';
		$(this.items).each(function(index){
			keyFrames += '@-webkit-keyframes ' + (conveyorId + "-" + index) + '{\n';
			for(var i = 0; i <= length; i++){
				if(i<=length){
					keyFrames += i * percent + '%    ' + '{left:0px; top:'+(i-1)*height +'px;z-index : 1}\n';
				}else{
					keyFrames += i * percent + '%    ' + '{left:0px; top:'+(i-1)*height +'px;z-index : -1}\n';
				}
			}
			keyFrames += '100%    ' + '{left:0px; top:'+(-1)*height +'px;z-index : -1}\n';
			keyFrames += '}\n';
			
			
			
			$(this).css("-webkit-animation-name", conveyorId + "-" + index);
			$(this).css("-webkit-animation-duration", duration);
			$(this).css("animation-duration", duration + "s");
			$(this).css("animation-name", conveyorId + "-" + index );
			$(this).css("animation-delay", duration / (length) * (length - index) + "s");
			$(this).css("animation-iteration-count", "infinite");
			
		})
		keyFrames += '</style>';
		var style = $("#" + this.conveyorId).html();
		console.log(style);
		$("#" + this.conveyorId).html(style + keyFrames);
		console.log($("style").html());
	}
	
	this.runConveyor = function(){
		this.getItems();
		this.setStyleItems();
		this.setStyleConveyor();
		this.setKeyFrameItems();
	}
}